import * as React from "react";

export const Separator: React.FC = () => <>, </>;
export const Dot: React.FC = () => <>. </>;
