export function getFirstNameInitials(firstName: string): string {
  return firstName
    .split("-")
    .map((nameComponent, index) => {
      if (index == 0) {
        return nameComponent.charAt(0) + ".";
      } else {
        return "-" + nameComponent.charAt(0) + ".";
      }
    })
    .join("");
}
