import React from "react";
import { Status } from "../../common/Status";
import { ErrorComponent } from "./ErrorComponent";
import { Loading } from "./Loading";

export const StatusComponent: React.FC<{ status: Status; children: React.ReactNode }> = ({ status, children }) => {
  switch (status) {
    case Status.OK:
      return <>{children}</>;
    case Status.LOADING:
      return <Loading />;
    case Status.ERROR:
    default:
      return <ErrorComponent />;
  }
};
