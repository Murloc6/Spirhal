import * as React from "react";
import styled from "styled-components";

import { NormContext } from "./Spirhal";
import { normRenderedGroups } from "./Group";

import { EHESSNotice } from "./norms/EHESS/EHESS";
import { APANotice } from "./norms/APA/APA";
import { MLANotice } from "./norms/MLA/MLA";

import pdfImg from "../images/file-pdf-solid.svg";

export interface Notice {
  docid: string;
  citationFull_s: string;
  authFirstName_s: string[];
  authLastName_s: string[];
  title_s: string;
  bookTitle_s: string | undefined;
  journalTitle_s: string | undefined;
  conferenceTitle_s: string | undefined;
  scientificEditor_s: string | undefined;
  volume_s: string | undefined;
  issue_s: string | undefined;
  authorityInstitution_s: string | undefined;
  publicationLocation_s: string | undefined;
  city_s: string | undefined;
  country_s: string | undefined;
  publisher_s: string | undefined;
  producedDateY_i: string | undefined;
  page_s: string | undefined;
  inPress_bool: boolean | undefined;
  proceedings_s: string | undefined;
  conferenceStartDateY_i: string | undefined;
  subTitle_s: string | undefined;
  otherType_s: string | undefined;
  files_s: string[] | undefined;
  fileAnnexes_s: string[] | undefined;
  uri_s: string | undefined;
  halId_s: string;
  docType_s: string;
}

const HALNoticeContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

const GenericNoticeContainer = styled.div`
  display: initial;
  width: 95%;
  padding-right: 20px;
`;
const IntegralTextContainer = styled.div`
  width: 5%;
  & img {
    width: 2rem;
  }
`;

export const EmptyNotice = () => <span>Aucune notice présente sur HAL</span>;

export enum Norm {
  EHESS = "EHESS",
  APA = "APA",
  MLA = "MLA",
  DEFAULT = "default",
}

const HALNotice: React.FC<{ citationFull_s: string }> = ({ citationFull_s }) => {
  return <span dangerouslySetInnerHTML={{ __html: citationFull_s }}></span>;
};

export const GenericNoticeComponent: React.FC<Notice> = (notice) => {
  const norm = React.useContext(NormContext);
  let noticeComponent = <></>;
  switch (norm) {
    case Norm.EHESS:
      noticeComponent = <EHESSNotice {...notice} />;
      break;
    case Norm.APA:
      noticeComponent = <APANotice {...notice} />;
      break;
    case Norm.MLA:
      noticeComponent = <MLANotice {...notice} />;
      break;
    default:
      noticeComponent = <HALNotice citationFull_s={notice.citationFull_s} />;
      break;
  }
  return noticeComponent;
};

export function useFilteredNotice(notice: Notice) {
  const norm = React.useContext(NormContext);
  const groupId = normRenderedGroups[norm]?.includes(notice.docType_s) ? notice.docType_s : "DEFAULT";
  const filteredNotice = { ...notice };
  if (filteredNotice.inPress_bool) {
    filteredNotice["producedDateY_i"] = "À paraître";
  } else if (groupId === "COMM" && filteredNotice.proceedings_s === "0") {
    filteredNotice["producedDateY_i"] = filteredNotice.conferenceStartDateY_i;
  }
  if (Array.isArray(filteredNotice.title_s)) {
    filteredNotice["title_s"] = filteredNotice.title_s[0]; // we consider the first element to be the french one
  }
  if (filteredNotice.subTitle_s !== undefined) {
    filteredNotice["title_s"] = `${filteredNotice.title_s} : ${filteredNotice.subTitle_s}`;
  }
  return filteredNotice;
}

export const NoticeComponent: React.FunctionComponent<Notice> = (notice) => {
  const filteredNotice = useFilteredNotice(notice);
  let urlFile = null;
  if (filteredNotice.files_s !== undefined && filteredNotice.files_s.length > 0) {
    urlFile = filteredNotice.files_s[0];
  } else if (filteredNotice.fileAnnexes_s !== undefined && filteredNotice.fileAnnexes_s.length > 0) {
    urlFile = filteredNotice.fileAnnexes_s[0];
  }
  return (
    <NoticeTypeContext.Provider value={{ noticeType: filteredNotice.docType_s }}>
      <HALNoticeContainer className="spirhal-notice">
        <GenericNoticeContainer className="spirhal-notice-citation">
          <a className="spirhal-notice-link" href={filteredNotice.uri_s}>
            <GenericNoticeComponent {...filteredNotice} />
          </a>
        </GenericNoticeContainer>
        <IntegralTextContainer className="spirhal-notice-fulltext-link">
          {urlFile !== null && (
            <a href={urlFile} className="spirhal-integral-text">
              <img src={pdfImg} />
            </a>
          )}
        </IntegralTextContainer>
      </HALNoticeContainer>
    </NoticeTypeContext.Provider>
  );
};

export const NoticeTypeContext = React.createContext<{ noticeType: string }>({ noticeType: "" });
