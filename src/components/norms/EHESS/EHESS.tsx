import * as React from "react";

import * as countries from "i18n-iso-countries";
// @ts-ignore
countries.registerLocale(require("i18n-iso-countries/langs/fr.json"));

import { Authors } from "./Author";
import { Notice, NoticeTypeContext } from "../../Notice";
import { Separator } from "../../common/Separator";
import Italic from "../../common/Italic";

export const renderedGroups = ["OUV", "COUV", "ART", "COMM", "DOUV", "POSTER", "THESE", "HDR"];

export const Title: React.FC<{
  title: string;
  bookTitle?: string;
  journalTitle?: string;
  conferenceTitle?: string;
}> = ({ title, bookTitle, journalTitle, conferenceTitle }) => {
  if (title.length === 0) {
    return <></>;
  }
  if (bookTitle !== undefined || journalTitle !== undefined || conferenceTitle !== undefined) {
    return <span>«{title}»</span>;
  }
  return <Italic>{title}</Italic>;
};

export const BookTitleAndScientificEditor: React.FC<{
  bookTitle?: string;
  journalTitle?: string;
  conferenceTitle?: string;
  scientificEditor?: string;
}> = ({ bookTitle, journalTitle, conferenceTitle, scientificEditor }) => {
  const { noticeType } = React.useContext(NoticeTypeContext);
  if (
    noticeType === "COUV" ||
    (bookTitle !== undefined &&
      journalTitle === undefined &&
      conferenceTitle === undefined &&
      (noticeType === "DEFAULT" || noticeType === "DOUV"))
  ) {
    const ret: JSX.Element[] = [];
    if (scientificEditor !== undefined) {
      ret.push(
        <>
          <Italic> in </Italic>
          {scientificEditor} (dir.)
          <Separator />
        </>
      );
    }
    if (bookTitle !== undefined) {
      ret.push(
        <>
          <Italic> in {bookTitle}</Italic>
          <Separator />
        </>
      );
    }
    return (
      <>
        {ret.map((val, i) => (
          <span key={i}>{val}</span>
        ))}
      </>
    );
  }
  return <></>;
};

export const JournalTitleAndVolumeAndIssue: React.FC<{
  journalTitle?: string;
  volume?: string;
  issue?: string;
}> = ({ journalTitle, volume, issue }) => {
  const { noticeType } = React.useContext(NoticeTypeContext);
  if (noticeType === "ART" || (journalTitle !== undefined && (noticeType === "DEFAULT" || noticeType === "DOUV"))) {
    return (
      <>
        {journalTitle !== undefined && (
          <span>
            <Italic>{journalTitle}</Italic>
            <Separator />
          </span>
        )}
        {volume !== undefined && (
          <span>
            vol. {volume}
            <Separator />
          </span>
        )}
        {issue !== undefined && (
          <span>
            n°{issue}
            <Separator />
          </span>
        )}
      </>
    );
  }
  return <></>;
};

const AuthorityInstitution: React.FC<{ authorityInstitution: string | undefined }> = ({ authorityInstitution }) => {
  const { noticeType } = React.useContext(NoticeTypeContext);
  if ((noticeType !== "THESE" && noticeType !== "HDR") || authorityInstitution === undefined) {
    return <></>;
  }
  return (
    <>
      {authorityInstitution}
      <Separator />
    </>
  );
};

const ConferenceTitle: React.FC<{
  journalTitle: string | undefined;
  scientificEditor: string | undefined;
  conferenceTitle: string | undefined;
}> = ({ journalTitle, scientificEditor, conferenceTitle }) => {
  const { noticeType } = React.useContext(NoticeTypeContext);
  if (
    noticeType === "COMM" ||
    noticeType === "POSTER" ||
    (noticeType === "DEFAULT" && conferenceTitle !== undefined && journalTitle === undefined)
  ) {
    const ret: JSX.Element[] = [];
    ret.push(<Italic>in </Italic>);
    if (scientificEditor !== undefined) {
      ret.push(
        <>
          {scientificEditor} (dir.)
          <Separator />
        </>
      );
    }
    if (conferenceTitle !== undefined) {
      ret.push(
        <>
          <Italic>{conferenceTitle}</Italic>
          <Separator />
        </>
      );
    }
    if (ret.length > 1) {
      return (
        <>
          {ret.map((val, i) => (
            <span key={i}>{val}</span>
          ))}
        </>
      );
    }
  }
  return <></>;
};

const Location: React.FC<{
  journalTitle: string | undefined;
  publicationLocation: string | undefined;
  city: string | undefined;
  country: string | undefined;
}> = ({ journalTitle, publicationLocation, city, country }) => {
  const { noticeType } = React.useContext(NoticeTypeContext);
  if ((noticeType === "DOUV" && journalTitle === undefined) || noticeType !== "ART") {
    const ret: JSX.Element[] = [];
    if (publicationLocation !== undefined) {
      ret.push(
        <>
          {publicationLocation}
          <Separator />
        </>
      );
    } else if (city !== undefined) {
      ret.push(
        <>
          {city}
          <Separator />
        </>
      );
    }
    if (country !== undefined) {
      ret.push(
        <>
          {countries.getName(country, "fr", { select: "official" })}
          <Separator />
        </>
      );
    }
    return (
      <>
        {ret.map((val, i) => (
          <span key={i}>{val}</span>
        ))}
      </>
    );
  }
  return <></>;
};

const Page: React.FC<{
  year: string | undefined;
  page: string | undefined;
}> = ({ year, page }) => {
  const { noticeType } = React.useContext(NoticeTypeContext);
  if (noticeType !== "OUV" && noticeType !== "DEFAULT" && page !== undefined) {
    return (
      <>
        {year !== undefined ? <Separator /> : <></>}
        {page.indexOf("-") !== -1 ? `p. ${page}` : `${page} p.`}
      </>
    );
  }
  return <></>;
};

export const EHESSNotice: React.FC<Notice> = (notice) => {
  return (
    <>
      <Authors authLastName_s={notice.authLastName_s} authFirstName_s={notice.authFirstName_s} />
      <Separator />
      <Title
        title={notice.title_s}
        bookTitle={notice.bookTitle_s}
        journalTitle={notice.journalTitle_s}
        conferenceTitle={notice.conferenceTitle_s}
      />
      <Separator />
      <BookTitleAndScientificEditor
        bookTitle={notice.bookTitle_s}
        journalTitle={notice.journalTitle_s}
        conferenceTitle={notice.conferenceTitle_s}
        scientificEditor={notice.scientificEditor_s}
      />
      <JournalTitleAndVolumeAndIssue
        journalTitle={notice.journalTitle_s}
        volume={notice.volume_s}
        issue={notice.issue_s}
      />
      <AuthorityInstitution authorityInstitution={notice.authorityInstitution_s} />
      <ConferenceTitle
        journalTitle={notice.journalTitle_s}
        scientificEditor={notice.scientificEditor_s}
        conferenceTitle={notice.conferenceTitle_s}
      />
      <Location
        journalTitle={notice.journalTitle_s}
        publicationLocation={notice.publicationLocation_s}
        city={notice.city_s}
        country={notice.country_s}
      />
      {notice.publisher_s !== undefined ? (
        <>
          {notice.publisher_s}
          <Separator />
        </>
      ) : (
        <></>
      )}
      {notice.producedDateY_i !== undefined ? <>{notice.producedDateY_i}</> : <></>}
      <Page year={notice.producedDateY_i} page={notice.page_s} />
    </>
  );
};
