import * as React from "react";
interface AuthorNames {
  authLastName_s: string[];
  authFirstName_s: string[];
}

interface AuthorName {
  authLastName: string;
  authFirstName: string;
}

const Author: React.FC<AuthorName> = ({ authFirstName, authLastName }) => (
  <span className="author">
    <span className="last-name">{authLastName.toUpperCase()}</span> <span className="first-name">{authFirstName}</span>
  </span>
);

export const Authors: React.FC<AuthorNames> = ({ authLastName_s, authFirstName_s }) => {
  switch (authLastName_s.length) {
    case 1:
      return <Author authFirstName={authFirstName_s[0]} authLastName={authLastName_s[0]} />;

    case 2:
      return (
        <span>
          <Author authFirstName={authFirstName_s[0]} authLastName={authLastName_s[0]} /> et{" "}
          <Author authFirstName={authFirstName_s[1]} authLastName={authLastName_s[1]} />
        </span>
      );
    default:
      return (
        <span>
          <Author authFirstName={authFirstName_s[0]} authLastName={authLastName_s[0]} />,{" "}
          <Author authFirstName={authFirstName_s[1]} authLastName={authLastName_s[1]} /> et al.
        </span>
      );
  }
};
