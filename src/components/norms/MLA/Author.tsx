import * as React from "react";
interface AuthorNames {
  authLastName_s: string[];
  authFirstName_s: string[];
}

interface AuthorName {
  authLastName: string;
  authFirstName: string;
}

const AuthorMLA: React.FC<AuthorName> = ({ authFirstName, authLastName }) => (
  <>
    {authLastName}, {authFirstName}.
  </>
);

export const AuthorsMLA: React.FC<AuthorNames> = ({ authLastName_s, authFirstName_s }) => {
  if (authLastName_s.length === 0) {
    return <></>;
  }
  if (authLastName_s.length <= 4) {
    return (
      <>
        {authLastName_s
          .map((lastName, i) => {
            const firstName = authFirstName_s[i];
            return (
              <>
                <AuthorMLA key={i} authFirstName={firstName} authLastName={lastName} />
              </>
            );
          })
          .reduce((accumulator: JSX.Element[], current) => {
            if (accumulator.length !== 0) {
              accumulator.push(<> </>);
            }
            accumulator.push(current);
            return accumulator;
          }, [])}
      </>
    );
  }
  return (
    <>
      {authLastName_s[0]}, {authFirstName_s[0]} et al.
    </>
  );
};
