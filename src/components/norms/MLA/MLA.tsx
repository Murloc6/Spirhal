import * as React from "react";

import * as countries from "i18n-iso-countries";
// @ts-ignore
countries.registerLocale(require("i18n-iso-countries/langs/fr.json"));

import { AuthorsMLA } from "./Author";
import { Notice, NoticeTypeContext } from "../../Notice";

export const renderedGroups = ["OUV", "COUV", "ART", "COMM", "DOUV"];

export const Title: React.FC<{
  title: string;
  subTitle?: string;
}> = ({ title, subTitle }) => {
  const { noticeType } = React.useContext(NoticeTypeContext);
  if (title.length === 0) {
    return <></>;
  }
  if (noticeType === "DOUV") {
    return <span>{title}.</span>;
  } else if (noticeType === "OUV") {
    return (
      <span>
        {title}
        {subTitle !== undefined && <> : {subTitle}</>}.
      </span>
    );
  }
  return <span>&quot;{title}&quot;</span>;
};

const TitleAndAuthor: React.FC<{
  title: string;
  authLastName_s: string[];
  authFirstName_s: string[];
  subTitle?: string;
}> = ({ title, subTitle, authLastName_s, authFirstName_s }) => {
  const { noticeType } = React.useContext(NoticeTypeContext);
  if (noticeType === "DOUV") {
    return (
      <>
        <AuthorsMLA authLastName_s={authLastName_s} authFirstName_s={authFirstName_s} />{" "}
        <Title title={title} subTitle={subTitle} />{" "}
      </>
    );
  }
  return (
    <>
      <Title title={title} subTitle={subTitle} />{" "}
      <AuthorsMLA authLastName_s={authLastName_s} authFirstName_s={authFirstName_s} />{" "}
    </>
  );
};

const BookTitle: React.FC<{
  bookTitle: string | undefined;
}> = ({ bookTitle }) => {
  const { noticeType } = React.useContext(NoticeTypeContext);
  if (noticeType === "COUV" && bookTitle !== undefined) {
    return <>{bookTitle}.</>;
  }
  return <></>;
};

const ScientificEditor: React.FC<{
  scientificEditor: string | undefined;
}> = ({ scientificEditor }) => {
  const { noticeType } = React.useContext(NoticeTypeContext);
  if ((noticeType === "COUV" || noticeType === "DOUV") && scientificEditor !== undefined) {
    return <>{scientificEditor}.</>;
  }
  return <></>;
};

const PublicationLocationAndPublisher: React.FC<{
  publicationLocation: string | undefined;
  publisher: string | undefined;
}> = ({ publicationLocation, publisher }) => {
  if (publicationLocation !== undefined && publisher !== undefined) {
    return (
      <>
        {publicationLocation} : {publisher}
      </>
    );
  }
  if (publicationLocation !== undefined) {
    return <>{publicationLocation}</>;
  }
  if (publisher) {
    return <>{publisher}</>;
  }
  return <></>;
};

const ProducedDate: React.FC<{
  producedDate: string | undefined;
}> = ({ producedDate }) => {
  if (producedDate !== undefined) {
    return <>, {producedDate}</>;
  }
  return <></>;
};

// TODO : regroup Page and InPress component to put the point at the end
const Page: React.FC<{ page: string | undefined }> = ({ page }) => {
  const { noticeType } = React.useContext(NoticeTypeContext);
  if (page !== undefined && noticeType === "COUV") {
    return <>{page}.</>;
  }
  return <></>;
};

const InPress: React.FC<{ inPress: boolean | undefined }> = ({ inPress }) => {
  if (inPress !== undefined && inPress) {
    return <>, Imprimé.</>;
  }
  return <></>;
};

const isOUVorDOUVorCOUV = (noticeType: string): boolean =>
  noticeType === "OUV" || noticeType === "DOUV" || noticeType === "COUV";

export const MLANotice: React.FC<Notice> = (notice) => {
  const { noticeType } = React.useContext(NoticeTypeContext);
  return (
    <>
      <TitleAndAuthor
        title={notice.title_s}
        subTitle={notice.subTitle_s}
        authLastName_s={notice.authLastName_s}
        authFirstName_s={notice.authFirstName_s}
      />
      <BookTitle bookTitle={notice.bookTitle_s} />
      <ScientificEditor scientificEditor={notice.scientificEditor_s} />
      {isOUVorDOUVorCOUV(noticeType) && (
        <>
          <PublicationLocationAndPublisher publicationLocation={notice.publisher_s} publisher={notice.publisher_s} />
          <ProducedDate producedDate={notice.producedDateY_i} />
          <Page page={notice.page_s} />
          <InPress inPress={notice.inPress_bool} />
        </>
      )}
    </>
  );
};
