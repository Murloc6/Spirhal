import * as React from "react";
import { getFirstNameInitials } from "../../common/utils";
interface AuthorNames {
  authLastName_s: string[];
  authFirstName_s: string[];
}

interface AuthorName {
  authLastName: string;
  authFirstName: string;
}

const AuthorAPA: React.FC<AuthorName> = ({ authFirstName, authLastName }) => (
  <span className="author">
    <span className="last-name">{authLastName}</span>,{" "}
    <span className="first-name">{getFirstNameInitials(authFirstName)}</span>
  </span>
);
// TODO: CD: rename as Author ?
export const AuthorsAPA: React.FC<AuthorNames> = ({ authLastName_s, authFirstName_s }) => {
  const children = authLastName_s.map((_, index, lastnameArray) => {
    const firstName = authFirstName_s[index];
    const lastName = authLastName_s[index];
    const authorJSX = <AuthorAPA authFirstName={firstName} authLastName={lastName} />;
    if (index + 2 < lastnameArray.length) {
      return <span key={index}>{authorJSX}, </span>;
    }
    if (index + 2 === lastnameArray.length) {
      return <span key={index}>{authorJSX} et </span>;
    }
    return <span key={index}>{authorJSX}</span>;
  });
  return <>{children}</>;
};
