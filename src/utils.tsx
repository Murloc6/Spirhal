export const publicationTypeEquivalent = new Map<string, string>([
  ["OUV", "Ouvrage"],
  ["COUV", "Chapitre d'ouvrage"],
  ["UNDEFINED", "Pré-publication"],
  ["ART", "Article"],
  ["COMM", "Communication dans un colloque"],
  ["OTHER", "Autre"],
  ["DOUV", "Direction d'ouvrage et coord. dossier (revue)"],
  ["REPORT", "Rapport"],
  ["MAP", "Carte"],
  ["POSTER", "Poster"],
  ["THESE", "Thèse"],
  ["IMG", "Image"],
  ["HDR", "Habilitation à diriger des recherches"],
  ["PATENT", "Brevet"],
  ["VIDEO", "Video"],
  ["OTHER1", "Article de blog"],
  ["OTHER2", "Compte-rendu d'ouvrage et note de lecture"],
  ["OTHER3", "Notice d'encyclopédie ou de dictionnaire"],
  ["OTHER4", "Traduction"],
  ["SOFTWARE", "Logiciel"],
  ["TRAD", "Traduction"],
  ["ISSUE", "N° spécial de revue"],
  ["NOTICE", "Notice"],
  ["PROCEEDINGS", "Actes de colloque"],
  ["BLOG", "Article de Blog"],
]);

export const groupOrder: string[] = [
  "OUV",
  "COUV",
  "DOUV",
  "ART",
  "COMM",
  "HDR",
  "THESE",
  "REPORT",
  "OTHER1",
  "OTHER2",
  "OTHER3",
  "OTHER4",
  "MAP",
  "POSTER",
  "PROCEEDINGS",
  "ISSUE",
  "IMG",
  "PATENT",
  "VIDEO",
  "SOFTWARE",
  "NOTICE",
  "UNDEFINED",
  "TRAD",
  "BLOG",
  "OTHER",
];

const allFetchedFields =
  "title_s," +
  "page_s," +
  "country_s," +
  "authFullName_s," +
  "subTitle_s," +
  "authorityInstitution_s," +
  "producedDateY_i," +
  "conferenceTitle_s," +
  "publisher_s," +
  "reportType_s," +
  "source_s," +
  "structName_s," +
  "uri_s," +
  "halId_s," +
  "version_i," +
  "citationFull_s," +
  "citationRef_s," +
  "docid," +
  "bookTitle_s," +
  "bookCollection_s," +
  "publicationLocation_s," +
  "scientificEditor_s," +
  "journalTitle_s," +
  "volume_s," +
  "issue_s," +
  "conferenceTitle_s," +
  "conferenceStartDateY_i," +
  "city_s," +
  "conferenceOrganizer_s," +
  "files_s," +
  "fileAnnexes_s," +
  "thumbId_i," +
  "journalPublisher_s," +
  "journalUrl_s," +
  "authFirstName_s," +
  "authLastName_s," +
  "inPress_bool," +
  "proceedings_s," +
  "conferenceStartDateY_i," +
  "otherType_s," +
  "publisherLink_s," +
  "docType_s," +
  "doiId_s";

export type StructId = number | number[] | null;

const addStructIdToUrl = (structId: StructId): string => {
  if (structId === null) {
    return "";
  }
  if (Array.isArray(structId)) {
    return `(${structId.map((id) => `structId_i:${id}`).join(" OR ")})`;
  }
  return `structId_i:${structId}`;
};

export const buildHALApiUrl = (
  portal: string,
  structId: StructId,
  limit: number,
  noGrouping: boolean,
  researcherName?: string,
  idHal?: string,
  baseUrl = "https://api.archives-ouvertes.fr"
): string => {
  let query = "";
  // use idhal first and then researcher name if idhal not defined
  if (idHal !== undefined) {
    // if idHal is not a number, use authIdHal_s
    if (isNaN(parseInt(idHal))) {
      query = `authIdHal_s:${idHal}`;
    } else {
      query = `authIdHal_i:${idHal}`;
    }
  } else if (researcherName !== undefined) {
    query = `authFullName_t:"${researcherName}"`;
  }
  // Only add structId restriction if idHal is not defined
  if (structId !== null && idHal === undefined) {
    query += query !== "" ? ` AND ${addStructIdToUrl(structId)}` : addStructIdToUrl(structId);
  }
  let groupingQuery = "";
  if (noGrouping) {
    groupingQuery = `&rows=${limit}`;
  } else {
    groupingQuery = `&group=true&group.field=docType_s&group.limit=${limit}`;
  }
  return (
    `${baseUrl}/search/${portal}` +
    `?q=${encodeURI(query)}` +
    `&wt=json&sort=producedDate_tdate%20desc` +
    `&fl=${allFetchedFields}` +
    `${groupingQuery}`
  );
};
