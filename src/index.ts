import * as React from "react";
import * as ReactDOM from "react-dom/client";
import {
  Spirhal,
  SpirhalProps,
  SpirhalStructure,
  NormContext,
  useHALGroups,
  SpirhalStructureProps,
} from "./components/Spirhal";

class GenericSpirhal extends HTMLElement {
  async getGenericSpirhalProps(): Promise<SpirhalProps> {
    let spirhalProps: SpirhalProps = {
      portal: this.getAttribute("portal") ?? "",
      structId: JSON.parse(this.getAttribute("struct-id") ?? "null"),
      norm: this.getAttribute("norm") ?? "default",
      adminLink: this.getAttribute("adminLink") ?? undefined,
      noGrouping: JSON.parse(this.getAttribute("noGrouping") ?? "false") === true ? true : false,
    };
    if (this.hasAttribute("backendURL") && this.hasAttribute("laboName")) {
      const labosJsonURL = this.getAttribute("backendURL") + "/labos";
      const laboName = this.getAttribute("laboName");
      const labos_response = await fetch(labosJsonURL!, { mode: "cors" });
      const labos = await labos_response.json();
      const selectedLabo = labos.find((labo: any) => labo.name === laboName);
      if (!selectedLabo) {
        throw new Error(`Selected labo '${laboName}' does not exist in ${labosJsonURL}`);
      }
      spirhalProps = { ...spirhalProps, structId: selectedLabo.struct_id, norm: selectedLabo.norm };
    }
    return spirhalProps;
  }
}

class SpirhalTag extends GenericSpirhal {
  async connectedCallback() {
    let spirhalProps: SpirhalProps;
    try {
      spirhalProps = await this.getGenericSpirhalProps();
    } catch (e: unknown) {
      console.warn((e as Error).message);
      return;
    }
    const researcherName = this.getAttribute("researcher-name");
    const idHal = this.getAttribute("idhal");
    const backendURL = this.getAttribute("backendURL");
    if (idHal !== null) {
      spirhalProps.idHal = idHal;
    } else if (researcherName !== null) {
      spirhalProps.researcherName = researcherName;
      if (backendURL !== null) {
        const halIdURL = new URL(backendURL + "/halid");
        halIdURL.searchParams.set("name", researcherName);
        if (spirhalProps.structId !== null) {
          if (Array.isArray(spirhalProps.structId)) {
            spirhalProps.structId.forEach((elem) => {
              halIdURL.searchParams.append("struct_ids", elem.toString());
            });
          } else {
            halIdURL.searchParams.append("struct_ids", spirhalProps.structId.toString());
          }
        }
        const halIdResponse = await fetch(halIdURL, { mode: "cors" });
        const newIdHal = await halIdResponse.text();
        if (newIdHal !== "null") {
          spirhalProps.idHal = newIdHal;
        }
      }
    } else {
      console.error("researcher-name or idhal must be provided for a researcher");
      return;
    }

    const mountPoint = document.createElement("div");
    mountPoint.setAttribute("id", "SpirhalApp");
    this.appendChild(mountPoint);
    const root = ReactDOM.createRoot(mountPoint);
    root.render(React.createElement(Spirhal, spirhalProps));
  }
}
customElements.define("spirhal-app", SpirhalTag);

class SpirhalStructureTag extends GenericSpirhal {
  async connectedCallback() {
    const limit = this.getAttribute("limit") !== null ? this.getAttribute("limit")! : "20";
    const structId = this.getAttribute("struct-id");
    const backendURL = this.getAttribute("backendURL");
    const laboName = this.getAttribute("laboName");
    console.log(limit, structId, backendURL, laboName);

    if (structId !== null || (backendURL !== null && laboName !== null)) {
      let spirhalProps: SpirhalProps;
      try {
        spirhalProps = await this.getGenericSpirhalProps();
      } catch (e: unknown) {
        console.warn((e as Error).message);
        return;
      }
      const spirhalStructureProps: SpirhalStructureProps = {
        ...spirhalProps,
        limit: Number.parseInt(limit),
      };
      const mountPoint = document.createElement("div");
      mountPoint.setAttribute("id", "SpirhalStructureApp");
      this.appendChild(mountPoint);
      const root = ReactDOM.createRoot(mountPoint);
      root.render(React.createElement(SpirhalStructure, spirhalStructureProps));
      return;
    }
    console.error("Provide struct-id OR (backendURL AND laboName)");
  }
}
customElements.define("spirhal-app-structure", SpirhalStructureTag);

export { SpirhalStructure };
export { Spirhal, NormContext, useHALGroups };
export { Notice, GenericNoticeComponent, NoticeTypeContext } from "./components/Notice";
export { useFilteredHalGroups, HALGroup } from "./components/Group";
