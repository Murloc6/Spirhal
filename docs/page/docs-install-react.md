---
title: Installation dans une application React
---

Pré-requis
==========

Ce mode d'utilisation de la bibliothèque nécessite d'être intégré dans une application
React déjà existante avec une chaine de compilation déjà configurée
([tutoriel React](https://fr.reactjs.org/docs/getting-started.html)).

Installation
============

L'application React est disponible sur npmjs.com. Il est donc possible
de l'installer directement avec npm :

```
    npm install --save spirhal
```

Ou bien encore avec yarn :

```
    yarn add --save spirhal
```

Affichage pour une personne
===========================

De la même manière que pour l'utilisation sur une page Web, il suffit
d'importer le composant et de l'insérer dans le JSX :

```
    import React from "react";
    import {SpirhHAL} from "spirhal";

    function App() {
        return (
            <div>
                <Spirhal idHal="42" noGrouping />
            </div>
        );
    }
```

Les paramètres disponibles sont :

- `structId`: Identifiant HAL du laboratoire de la personne
  - peut être une liste d'identifiants HAL (e.g. `structId="[620,42]"`)
  - peut être laissé vide s'il n'est pas connu
- `norm`: nom de la norme bibliographique dans laquelle les notices doivent être présentées
  - choix entre APA, MLA, EHESS
  - possibilité de laisser vide pour la citation de HAL par défaut
- `idHal`: Identifiant HAL de la personne
- `researcherName`: nom complet de la personne
  - utilisé si aucun identifiant HAL n'est renseigné
- `portal`: si un portail HAL est à renseigner pour la recherche
  - peut-être laissé vide
- `adminLink`: lien du site d'administration SpirHAL pour la personne, s'il est déployé
- `noGrouping`: si ajouté, les publications seront toutes affichées sans être groupées par type de document
  - e.g. `<spirhal ... noGrouping />`

Affichage pour une structure
============================

De la même manière il est possible d\'afficher les notices pour une
structure donnée :

```
    import React from "react";
    import SpirhalStructure from "spirhal";

    function App() {
        return (
            <div>
                <SpirhalStructure struct-id="620" norm="EHESS" limit="20" noGrouping />
            </div>
        );
    }
```

Les paramètres suivants sont disponibles sur cette balise :

- `structId`: Identifiant HAL de la structure
  - nécessaire
- `norm`: nom de la norme bibliographique dans laquelle les notices doivent être présentées
  - choix entre APA, MLA, EHESS
  - possibilité de ne pas le renseigner pour la citation de HAL par défaut
- `limit`: nombre de notices maximales à récupérer par type de notice
- `portal`: si un portail HAL est à renseigner pour la recherche des notices
  - peut-être laissé vide
- `adminLink`: lien du site d'administration SpirHAL, s'il est déployé
- `noGrouping`: si ajouté, les publications seront toutes affichées sans être groupées par type de document
