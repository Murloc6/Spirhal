---
title: A propos
---

Le projet SpirHAL est l'initiative du groupe de travail Open Access de
l'université Toulouse Jean Jaurès. L'objectif de ce projet a été de
développer un outil facile d'utilisation et d'installation pour
permettre l'affichage du contenu de
[HAL](https://hal.archives-ouvertes.fr/) sur la page annuaire de chaque
chercheur. De cette manière la bibliographie des membres de laboratoire
est plus simple à maintenir et l'affichage est homogène sur toutes les
pages annuaires d'un même laboratoire.

Ce projet a ensuite évolué pour permettre la détection de champs
manquants (champs nécessaire à l'affichage de la citation en suivant la
norme déterminée par le laboratoire) et des doublons (entre une citation
présente dans HAL et sur la fiche annuaire).

l'entreprise Murloc WebDev (Fabien Amarger) a développé ce projet en
collaboration avec le groupe de travail Open Access.
