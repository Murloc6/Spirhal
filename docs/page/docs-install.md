---
title: Installation sur une page Web
---

Pré-requis
==========

Pour installer l'affiche des notices de HAL pour un membre il est
nécessaire d'avoir un site hébergé. Ce module vient s'intégrer dans la
page pour afficher les notices d'un membre donné ou d\'une structure
donnée. Aucune autre pré requis n'est nécessaire.

Installation
============

Récupération du script
----------------------

Pour récupérer SpirHAL vous avez deux possibilités :

### CDN

La première possibilité est d'utiliser directement la dernière version
de SpirHAL sur un serveur qui l'héberge :
```
    https://cdn.jsdelivr.net/npm/spirhal@latest/dist/spirhal.min.js
```

Si pour des raisons de rétro-compatibilité vous souhaitez utiliser une
version spécifique de SpirHAL, il est possible de récupérer cette
version en utilisant l'URL suivante :

```
    https://cdn.jsdelivr.net/npm/spirhal@{VERSION}/dist/spirhal.min.js
```

Par exemple pour récupérer la version `v4.3.1` vous pouvez
utiliser l'URL suivante :

    https://cdn.jsdelivr.net/npm/spirhal@v4.3.1/dist/spirhal.min.js

Cette méthode peut permettre de fixer une version et éviter des problème
de compatiblité lors des prochaines mises à jours de SpirHAL.

### Compilation

La dernière possibilité est de compiler vous même SpirHAL. Pour compiler
vous même le script il est nécessaire d'avoir installé
[Node.JS](https://nodejs.org/fr/) et [GIT](https://git-scm.com/) sur
votre ordinateur.

Tout d'abord il vous faut cloner le git du projet SpirHAL :

```
    git clone git@framagit.org:Murloc6/Spirhal.git
```

Une fois le contenu du projet récupérer, il faut se rendre dans le
dossier et lancer la commande de compilation :

```
    cd Spirhal
    npm ci
    npm run build:prod
```

Lorsque le processus est terminé, les fichiers compilés de SpirHAL sont
disponibles dans le dossier dist. Notament le fichier
`bundle.js` qui contient tout ce qui est nécessaire pour
faire fonctionner SpirHAL.

Importer les fichiers sur votre page web
----------------------------------------

Maintenant que vous avez les fichiers compilés de SpirHAL (soit par CDN
soit en les compilant vous même) il vous faut les importer dans le code
de votre page web. Pour cela il faut rajouter les balises d'import dans
le `head` de votre page web. Par exemple si nous avons utilisé la
version hébergée sur le serveur CDN, le script à importer est :

```
    <script src="https://cdn.jsdelivr.net/npm/spirhal@latest/dist/spirhal.min.js"></script>
```

Ce scripts importé contient toutes les dépendances nécessaires à SpirHAL
pour fonctionner.

Affichage pour une personne
===========================

Une fois le fichier importé il faut mainteenant initialiser SpirHAL.
Pour cela, SpirHAL utilise le principe de "WebComponent" (plus
d'informations
[ici](https://developer.mozilla.org/fr/docs/Web/Web_Components)). Ce qui
signifie que l'on utilise une balise spéficique pour SpirHAL avec les
paramètres correspondants. Ici la balise s'appelle "spirhal-app". Nous
pouvons alors insérer dans notre page cette balise et la paramétrer pour
qu'elle affiche les notices de la personne concernée. Par exemple si
nous souhaitons afficher les notices de "Hélène Débax" du laboratoire
Framespa (structID HAL 620) avec la norme "EHESS" alors nous pouvons
insérer la balise suivante :

```
    <spirhal-app researcher-name="Hélène Débax" norm="EHESS" struct-id="620"/>
```

On peut remarquer le nom de la balise est bien "spirhal-app" et ensuite
les paramètres sont des attributs de la balise :

- researcher-name est la personne dont on souhaite afficher les
    notices venant de HAL (attention de bien réspecter "Prénom Nom" dans
    cet ordre avec ces majuscules pour que la requête sur HAL fonctionne
    correctement)
- "norm" est le nom de la norme bibliographique à utiliser (seul les
    normes implémentées peuvent être utilisées). S'il n'y a pas de norme
    spécifiée alors SpirHAL utilisera la représentation utilisée sur HAL
- "struct-id" est le structId sur HAL du laboratoire auquel appartient
    la personne (ce paramètre est optionnel)
  - il est possible de connaitre le structId d'un laboratoire sur
    [AuréHAL](https://aurehal.archives-ouvertes.fr/structure/index)

Une fois cette balise ajoutée, les notices provenant de la personne
spécifiée dans "researcher-name" seront affichées à l'endroit où est la
balise.

Les paramètres suivants sont disponibles comme attributs de la balise :

- `backendURL`: l'URL de l'API d'un
    [Spirhal-backend](https://forge.extranet.logilab.fr/spirhal/spirhal-backend),
    utilisé pour la gestion des institutions et des récupérations
    d'idHal.
- `laboName`: le nom d'un laboratoire dans le JSON servi par l'URL
    `${backendURL}/labos`, utilisé pour récupérer les
    informations suivantes à la place des paramètres mis manuellement
    dans un spirhal: struct-id et norm
- `struct-id`: Identifiant HAL du laboratoire de la personne :
  - peut être une liste d\'identifiants HAL (e.g.
    `struct-id="[620,42]"`
  - peut être laissé vide s'il n\'est pas connu
  - dans l'API de HAL, l'utilisation de cet identifiant filtre
    les publications pour ne garder que celles qui ont au moins
    une personne attachée à ce structId
- `norm`: nom de la norme bibliographique dans laquelle les notices doivent être présentées
  - choix entre APA, MLA, EHESS
  - possibilité de laisser vide pour la citation de HAL par défaut
- `idhal`: Identifiant HAL de la personne, s'il est renseigné, cet
   identifiant est utilisé en priorité, il ignorera la valeur de
   structId pour afficher la totalité des publications de la personne
- `researcherName`: nom complet de la personne
  - utilisé si aucun identifiant HAL n\'est renseigné
- `portal`: si un portail HAL est à renseigner pour la recherche
  - peut-être laissé vide
- `adminLink`: lien du site d'administration SpirHAL, s'il est déployé
- `noGrouping`: si ajouté, les publications seront toutes affichées
   sans être groupées par type de document
  - e.g. `<spirhal-app ... noGrouping />`

Page complète
-------------

Un exemple de page complète pour "Hélène Débax" et ne contenant que la
liste des notices est la suivante, cf le fichier
`index.html` qui présente des utilisations de Spirhal avec
différents jeux de paramètres :
```html
    <html>
        <head>
            <script src="https://cdn.jsdelivr.net/npm/spirhal@latest/dist/spirhal.min.js"></script>
        </head>
        <body>
            <spirhal-app researcher-name="Hélène Débax" norm="EHESS" structId="620"/>
        </body>
    </html>
```


Affichage pour une structure
============================

Pour afficher les notices provenant d'une structure plutôt que d'une
personne (pour par exemple afficher toutes les notices sur la page
d'acceuil d'un laboratoire) il est possible d'utiliser la balise
`spirhal-app-structure`, cf fichier `index.html`
avec des exemples d'utilisation de SpirHAL :

```
    <spirhal-app-structure struct-id="620" norm="EHESS" limit="20" noGrouping />
```

Les paramètres suivants sont disponibles sur cette balise :

- `backendURL`: l'URL de l'API d'un
    [Spirhal-backend](https://forge.extranet.logilab.fr/spirhal/spirhal-backend),
    utilisé pour la gestion des institutions et des récupérations
    d'idHal.
- `laboName`: le nom d'un laboratoire dans le JSON servi par l\'URL
    `${backendURL}/labos`, utilisé pour récupérer les
    informations suivantes à la place des paramètres mis manuellement
    dans un spirhal: struct-id et norm
- `struct-id`: Identifiant HAL de la structure
  - nécessaire si backendURL et laboName ne sont pas renseignés
- `norm`: nom de la norme bibliographique dans laquelle les notices doivent être présentées
  - choix entre APA, MLA, EHESS
  - possibilité de ne pas le renseigner pour la citation de HAL par défaut
- `limit`: nombre de notices maximales à récupérer par type de notice
- `portal`: si un portail HAL est à renseigner pour la recherche des notices
  - peut-être laissé vide
- `adminLink`: lien du site d'administration SpirHAL, s'il est déployé
- `noGrouping`: si ajouté, les publications seront toutes affichées sans être groupées par type de document
