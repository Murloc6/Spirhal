# Modifier SpirHAL

Le projet SpirHAL étant du logiciel libre il est tout à fait possible de
modifier tous les éléments de ce projet. Nous allons ici présenter
comment modifier ou ajoute une nouvelle norme dans SpirHAL.

Le code source de SpirHAL est développé en utilisant la technologie
[React](https://fr.reactjs.org/) il est donc fortement recommandé de
maîtriser cette technologie avant de proposer des modifications à
SpirHAL.

## Modifier une norme bibliographique

Chaque norme bibliographique est proposée comme un "Component" React.
Toutes les normes sont présentes dans le dossier
"/src/components/spirhal/norms". Trois sont implémentées pour
l'instant: EHESS, APA, MLA.

La logique de formatage d'une notice suivant une norme est contenue
dans un sous-dossier de "/src/components/spirhal/norms" ayant le nom de
la norme. Chaque norme doit fournir un composant React pour affichage
d\'une notice selon la norme bibliographique dont les propriétés
implémentent l'interface Notice
(`src/components/spirhal/Notice.tsx`).

Une norme exporte aussi une liste renderedGroups de types
de notices qui nécessitent un traitement particulier par la norme pour
leur affichage. Les autres types de notices sont traités "par défaut"
par la norme.

```
export const renderedGroups = ["OUV", "COUV", "ART", "COMM", "DOUV", "POSTER", "THESE", "HDR"];

export const APANotice: React.FC<Notice> = (notice) => {
   ....

}
```
## Ajouter une norme bibliographique

Pour ajouter une nouvelle norme nommée `EXEMPLE` dans SpirHAL, il faut
créer un dossier `/src/components/spirhal/norms/EXEMPLE`. Dans celui-ci
créer un fichier `EXEMPLE.tsx` qui exportera un composant React comme
suit et la liste des types de notices suivant un traitement particulier
par la norme.

```
export const renderedGroups = ["OUV", "COUV", "ART", "COMM", "DOUV", "POSTER", "THESE", "HDR"];
export const EXEMPLENotice: React.FC<Notice> = (notice) => {
       ....

    }
```

D\'autres fichiers peuvent être créés dans le dossier `EXEMPLE` pour
contenir la logique propre à la norme.

Ensuite, dans `src/components/spirhal/Notice.tsx`, il faut:

- importer le composant EXEMPLENotice
- ajouter la valeur à l'énumération Norm
- ajouter un cas dans le switch de GenericNoticeComponent

```html
    import { EXEMPLENotice } from "./norms/EXEMPLE/EXEMPLE";

    enum Norm {
      EHESS = "EHESS",
      APA = "APA",
      MLA = "MLA",
      DEFAULT = "default",
      EXEMPLE = "EXEMPLE",
    }

    const GenericNoticeComponent: React.FC<Notice> = (notice) => {
      ...
      switch (norm) {
        case Norm.EXEMPLE:
          noticeComponent = <EXEMPLENotice {...notice} />;
          break;
      ...
      }
      ...
    }
```

Dans le fichier `src/components/spirhal/Groups.tsx`, il faut:

- importer le contenu de la norme (ou au moins la liste des
    renderedGroups)
- ajouter la liste des renderedGroups au dictionnaire
    normRenderedGroups

```html
    import * as EXEMPLE from "./norms/EXEMPLE/EXEMPLE";

    ...

    const normRenderedGroups: { [key: string]: string[] } = {
      EHESS: EHESS.renderedGroups,
      APA: APA.renderedGroups,
      MLA: MLA.renderedGroups,
      EXEMPLE: EXEMPLE.renderedGroups,
    };
```

Il faudra enfin utiliser le nom de la norme lors de l'implémentation de
SpirHAL pour utiliser cette nouvelle norme dans votre page de votre site
web avec :
```
    <spirhal researcherName="Hélène Débax" norm="EXEMPLE" structId="620"></spirhal>
```

### Modifier les noms des types de publication

Cette association est définie dans le fichier [`src/utils.tsx`](src/utils.tsx) dans la variable `publicationTypeEquivalent`.

### Modifier l'ordre d'affichage des types de publication

Cet ordre peut être modifié dans le fichier [`src/utils.tsx`](src/utils.tsx) dans la variable `groupOrder`.

### Modifier les champs récupérés

Les différents champs récupérés pour chaque notice peuvent être modifiés dans le fichier [`src/utils.tsx`](src/utils.tsx) dans la variable `allFetchedFields`.
