![](page/img/SPIRHAL.png)

[![pipeline
status](https://framagit.org/Murloc6/Spirhal/badges/master/pipeline.svg)](https://framagit.org/Murloc6/Spirhal/commits/master)

le code source de SpirHAL est disponible
[ici](https://framagit.org/Murloc6/Spirhal).

Plan de la documentation :

1. [Utilisation](page/docs-install)
    1. [Composant](page/docs-install)
    2. [React](page/docs-install-react)
2. [Développement](page/docs-dev)


Présentation du projet
======================

SpirHAL est un logiciel libre et open-source en JavaScript/React
permettant le déploiement d'un module d'affichage sur un site web des
notices de publications scientifiques figurant sur la plateforme
[HAL](https://hal.archives-ouvertes.fr/).

Affichage pour une personne
---------------------------

Ce mode permet l'affichage des notices des publications pour une
personne en respectant une norme bibliographique donnée. Il peut être
utilisé pour un affichage sur une fiche annuaire des membres d'un
laboratoire.

Affichage pour une structure
----------------------------

Ce mode permet l'affichage des notices des publications pour une
structure, identifiées par un structId dans HAL. Par exemple, FRAMESPA a
le structId 620 dans HAL:

```
<https://hal.archives-ouvertes.fr/search/index/q/*/structId_i/620/>
```

Pour commencer
==============

Il y a deux possibilités d\'intégrer SpirHAL. La première est
l'utilisation par WebComponent pour insérer SpirHAL dans une page Web
déjà existante :

[Documentation pour l\'utilisation de
WebComponent](page/docs-install).

La deuxième méthode est l'utilisation par un componsant React donc la
[documentation est disponible ici](page/docs-install-react).

À propos
========

le code source de SpirHAL est disponible
[ici](https://framagit.org/Murloc6/Spirhal).

Ce projet est développé avec la technologie React, et est disponible
sous forme de Web component, ce qui permet un déploiement facile et
rapide sur tout site web. L'utilisation de cette technologie permet
aussi la compatibilité avec la plupart des navigateurs.

Licence [Creative Commons Attribution-NonCommercial-ShareAlike 4.0
International Public
License](https://framagit.org/Murloc6/Spirhal/blob/master/LICENSE)
